package jp.alhinc.sato_keisuke.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class CalculateSales {
	static final String stfBranchLst = "branch.lst";
	static final String stfBranchOut = "branch.out";
	static final String stfSalesEx = "rcd";

	static final int stfCodeLen = 3;//支店コード桁数
	static final int stfLstItems = 2;//支店定義ファイル項目数
	static final int stfSalesFileNameLen = 8;//支店定義ファイル項目数

	/**
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		LinkedHashMap<String, String> branchMap = new LinkedHashMap<>();
		HashMap<String, Long> summaryMap = new HashMap<>();
		BufferedReader br = null;
		PrintWriter pw = null;
		String dir = "";
		try {
			if (args.length == 1) {
				dir = args[0];
			} else {
				return;
			}

			File branchFile = new File(dir, stfBranchLst);
			if (!noBranchFileException(branchFile)) {
				return;
			}

			br = new BufferedReader(new FileReader(branchFile));

			String line;
			while ((line = br.readLine()) != null) {
				String[] branchData = line.split(",", 0);
				if (!branchFormatException(branchData)) {
					return;
				}
				branchMap.put(branchData[0], branchData[1]);
				summaryMap.put(branchData[0], 0L);
			}

	        File files[] = new File(dir).listFiles();
	        ArrayList<File> rcdFileList = new ArrayList<>();

	        for(int i=0; i < files.length; i++){
	        	if (files[i].getName().matches("[0-9]{8}+\\." + stfSalesEx)){
	        		rcdFileList.add(files[i]);
	        	}
	        }

	        if (!rcdSerialException (rcdFileList)) {
	        	return;
	        }

	        for(int i=0; i < rcdFileList.size(); i++){

    			br = new BufferedReader(new FileReader(rcdFileList.get(i)));

    			String code = "";
    			long summary = 0L;
    			int lineCount = 0;

    			while ((code = br.readLine()) != null) {
    				if (!branchCodeException (code, branchMap, rcdFileList.get(i).getName())) {
    					return;
    				}

    				summary = Long.parseLong(br.readLine()) + summaryMap.get(code);

    				if (!summaryException(summary)) {
    					return;
    				}

    				summaryMap.put(code, summary);
    				lineCount++;
    			}

    			if (!rcdFormatException(lineCount, files[i].getName())) {
    				return;
    			}
	        }

	        new File(dir + File.separator + stfBranchOut).createNewFile();
	        pw = new PrintWriter(new BufferedWriter(new FileWriter(dir + File.separator + stfBranchOut)));

	        for (Entry<String, String> entry : branchMap.entrySet()) {
	            pw.print(entry.getKey() + "," + entry.getValue() + "," + summaryMap.get(entry.getKey()) + "\r\n");
	        }
	        System.out.println("complete");
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("予期せぬエラーが発生しました");

		} finally {
			try {
				if (br != null) {
					br.close();
				}
				if (pw != null) {
					pw.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 支店定義ファイルの有無チェック
	 * @param file
	 */
	private static boolean noBranchFileException(File file) {
		boolean result = true;
		if (file.exists()) {
		} else {
			System.out.println("支店定義ファイルが存在しません");
			result = false;
		}
		return result;
	}

	/**
	 * 支店定義ファイルのフォーマットを確認
	 * @param str １行単位の読取データ
	 */
	private static boolean branchFormatException(String[] branchData) {
		boolean result = true;
		if ((branchData[0].matches("[0-9]{" + stfCodeLen + "}")) && (branchData.length == stfLstItems)) {

		} else {
			result = false;
			System.out.println("支店定義ファイルのフォーマットが不正です");
		}
		return result;
	}

	/**
	 * 合計金額の桁数チェック
	 * @param str
	 */
	private static boolean summaryException(long summary) {
		boolean result = true;
		long limit = 9999999999L;
		if (summary > limit) {
			System.out.println("合計金額が10桁を超えました");
			result = false;
		}
		return result;
	}

	/**
	 *
	 * @param lineSize
	 * @param fileName
	 * @return
	 */
	private static boolean rcdFormatException(int lineSize, String fileName) {
		boolean result = true;
		if (lineSize > 1) {
			System.out.println("<" + fileName + ">のフォーマットが不正です");
			result = false;
		}
		return result;
	}

	/**
	 *
	 * @param code
	 * @param branchMap
	 * @param fileName
	 * @return
	 */
	private static boolean branchCodeException (String code, LinkedHashMap<String, String> branchMap, String fileName) {
		boolean result = true;
		if (!branchMap.containsKey(code)) {
			System.out.println("<" + fileName + ">の支店コードが不正です");
			result = false;
		}
		return result;
	}

	/**
	 *
	 * @param rcdFileList
	 * @return
	 */
	private static boolean rcdSerialException (ArrayList<File> rcdFileList) {
		boolean result = true;
		Collections.sort(rcdFileList);

		for (int i = 0; i < rcdFileList.size(); i++) {
			int fileName = Integer.parseInt(getPreffix(rcdFileList.get(i).getName()));
			if ((fileName != (i + 1)) || (rcdFileList.get(i).isDirectory())) {
				result = false;
				System.out.println("売上ファイル名が連番になっていません");
				break;
			}
		}
		return result;
	}

	/**
	 * 拡張子抜きのファイル名にする
	 * @param fileName 拡張子付きファイル名
	 * @return 拡張子なしのファイル名
	 */
	private static String getPreffix(String fileName) {
	    if (fileName == null)
	        return null;
	    int point = fileName.lastIndexOf(".");
	    if (point != -1) {
	        return fileName.substring(0, point);
	    }
	    return fileName;
	}
}
